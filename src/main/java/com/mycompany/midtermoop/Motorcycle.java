/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midtermoop;

/**
 *
 * @author ASUS
 */
public class Motorcycle extends Car {

    public Motorcycle(String name, String color, String engine, String body) {
        super(name, color, engine, body, 2);
        System.out.println("Motorcycle Created");
    }

    @Override
    public void move() {
        super.move();
        System.out.println("Motorcycle:" + name + "move with "
                + numberOfWheels + " Wheels");
    }

    @Override
    public void voice() {
        super.voice();
        System.out.println("Motorcycle:" + name + " voice > Vroom Vroom !! ");
    }

}
