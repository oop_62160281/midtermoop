/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midtermoop;

/**
 *
 * @author ASUS
 */
public class TestCar {
    public static void main(String[] args) {
        Car car = new Car("Ca ", "White ","Single Cylinder Engine "
                ,"Auto Body ",0);
        car.voice();
        car.move();
        
        Motorcycle honda = new Motorcycle("Honda ", "White & green"
                ,"Single Cylinder Engine ","Auto Body ");
        honda.voice();
        honda.move();
        
        Supercar Lamborghini = new Supercar("Lamborghini ", "Black"
                ,"Single Cylinder Engine ","Auto Body ");
        Lamborghini.voice();
        Lamborghini.move();
        Lamborghini.openCarDoor();
        
        Supercar Mclaren = new Supercar("Mclaren ", "Orange"
                ,"Single Cylinder Engine ","Auto Body ");
        Mclaren.voice();
        Mclaren.move();
        Mclaren.openCarDoor();
        
        System.out.println("honda is Car: " 
                + (honda instanceof Car));
        System.out.println("honda is Motorcycle: " 
                + (honda instanceof Motorcycle));
        System.out.println("honda is Motorcycle: " 
                + (honda instanceof Object));
        
        System.out.println("Lamborghini is Car: " 
                + (Lamborghini instanceof Car));
        System.out.println("Lamborghini is Supercar: " 
                + (Lamborghini instanceof Supercar));
        System.out.println("Lamborghini is Supercar: " 
                + (Lamborghini instanceof Object));
        
        System.out.println("Mclaren is Car: " 
                + (Mclaren instanceof Car));
        System.out.println("Mclaren is Supercar: " 
                + (Mclaren instanceof Supercar));
        System.out.println("Mclaren is Supercar: " 
                + (Mclaren instanceof Object));
    }
}
