/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midtermoop;

/**
 *
 * @author ASUS
 */
public class Car {

    protected String name;
    protected int numberOfWheels = 0;
    protected String color;
    protected String engine;
    protected String body;

    public Car(String name, String color, String engine, String body,
            int numnumberOfWheels) {
        System.out.println("Car Created");
        this.name = name;
        this.color = color;
        this.engine = engine;
        this.body = body;
        this.numberOfWheels = numnumberOfWheels;
    }

    public void move() {
        System.out.println("Car: move");
    }

    public void voice() {
        System.out.println("Car voice");
        System.out.println("name : " + "{" + this.name + "}" + "numberOfWheels : "
                + "{" + this.numberOfWheels + "}"
                + " color : " + "{" + this.color + "}"
                + "engine : " + "{" + this.engine + "}"
                + "body : " + "{" + this.body + "}");
    }

    public String getName() {
        return name;
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }

    public String getColor() {
        return color;
    }

    public String getEngine() {
        return engine;
    }

    public String getBody() {
        return body;
    }
    

}
