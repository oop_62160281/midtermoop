/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midtermoop;

/**
 *
 * @author ASUS
 */
public class Supercar extends Car {
    private int numberOfCarDoor;
    public Supercar(String name, String color, String engine, String body){
        super(name, color, engine, body, 4);
        System.out.println("Supercar Created");
        
    }
    public void openCarDoor(){
        System.out.println("Supercar : " + name + " **openCarDoor**");
    }
    
    @Override
    public void move() {
        super.move();
        System.out.println("Supercar:" + name + "move with "
                + numberOfWheels + " Wheels");
    }

    @Override
    public void voice() {
        super.voice();
        System.out.println("Supercar:" + name + " voice > Honk Honk !! ");
    }
    
}
